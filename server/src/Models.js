import mongoose from "mongoose";
import shortid from "shortid";

// user info model
const UserInfoModel = mongoose.model("user", {
	_id: { type: Number, id: true },
	displayname: String,
	pic: String,
	lastlogin: { type: Date, default: Date.now }
});

// room state model
const RoomModel = mongoose.model("room", {
	_id: {
		type: String,
		default: shortid.generate
	},
	name: String,
	media: [String],
	cur: Number,
	start: Number,
	persistent: { type: Boolean, default: false },
	owner: String
});

export { RoomModel, UserInfoModel };
