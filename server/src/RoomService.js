import chalk from "chalk";
import { status } from "./log";
// import { check, validationResult } from "express-validator/check";
import Room from "./Room";
import { RoomModel } from "./Models";
import { ClocksyServer } from "clocksy";
import socketio from "socket.io";
import socketpassport from "passport.socketio";
import cookieParser from "cookie-parser";
import _ from "lodash";

/**
 * console.log prepended with [RoomService]
 * @param {String} message
 */
function log(message) {
	console.log(`${chalk.blue("[RoomService]")} ${message}`);
}

/**
 * Service that manages the current rooms being served
 */
class RoomService {
	/** Rooms currently being served  */
	currentRooms = {};

	/**
	 * Starts the Room Service
	 * @param {socketio.Server} io socket.io server
	 */
	start(sessionConfig, server) {
		const io = socketio(server);
		this.io = io;
		io.use(
			socketpassport.authorize({
				cookieParser: cookieParser,
				key: sessionConfig.key,
				secret: sessionConfig.secret,
				store: sessionConfig.store,
				fail: this._onAuthorizationFail,
				success: this._onAuthorizationSuccess
			})
		);

		this.clock = new ClocksyServer();

		log("Starting...");

		// get room data from db

		log("Downloading room data from db...");

		RoomModel.find({}, (err, docs) => {
			if (err) {
				console.error("Error starting RoomService:");
				console.error("Failed to get room data from DB");
				console.error(err);
			} else {
				log(`Received data for ${docs.length} room(s)`);
				// load the rooms
				this.loadRooms(docs);
			}
		});
	}

	_onAuthorizationFail(data, message, error, authorize) {
		authorize(null, false);
	}

	_onAuthorizationSuccess(data, authorize) {
		authorize(null, true);
	}

	/**
	 * Load rooms from data
	 * @param {Array} docs room documents
	 */
	loadRooms(docs) {
		log(`Loading ${docs.length} room(s) from data...`);

		docs.forEach(doc => {
			this.currentRooms[doc._id] = new Room(this.io, this.clock, doc);
			status.rooms = Object.keys(this.currentRooms).length;
		});

		log(`Loaded ${docs.length} room(s)`);

		this.io.on("connection", socket => {
			log(`Client ${socket.id} connected`);
			status.connections++;

			socket.on("join", data => {
				this.joinRoom(socket, data.roomid);
			});

			socket.on("disconnect", () => {
				log(`Client ${socket.id} disconnected`);
				status.connections--;
			});
		});

		log(chalk.green("Service is now Online!"));
		status.roomservice = true;
	}

	joinRoom(socket, roomid) {
		if (!this.currentRooms.hasOwnProperty(roomid)) {
			// send '404 room not found' to client
			socket.emit("joinerror", "Room not found");
			socket.disconnect(true);
			return;
		}

		if (!this.currentRooms[roomid].open) {
			// send 'room not loaded yet' to client
			socket.emit("joinerror", "Room is not open");
			socket.disconnect(true);
			return;
		}

		this.currentRooms[roomid].join(socket);
	}

	createRoom(options) {
		console.log(options);
		let model = new RoomModel(options);

		this.currentRooms[model._id] = new Room(this.io, this.clock, model);
		status.rooms = Object.keys(this.currentRooms).length;
		//console.log(options.owner)

		return this.currentRooms[model._id];
	}
}

/**
 * Service that manages the current rooms being served
 */
export default new RoomService();
