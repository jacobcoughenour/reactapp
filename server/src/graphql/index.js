import { makeExecutableSchema } from "graphql-tools";
import typeDefs from "./schema.graphql";
import resolvers from "./Resolvers";

const schema = makeExecutableSchema({
	typeDefs: typeDefs,
	resolvers: resolvers
});

//Load all of the graphql queries, mutations, etc, here

// const schema = new GraphQLSchema({
// 	query: RootQuery,
// 	mutation: RootMutation
// });

// es6 export
export default schema;

// CommonJS export for schema.json generation
module.exports = schema;
