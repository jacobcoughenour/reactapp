import RoomService from "../RoomService";
import _ from "lodash";

export default {
	Query: {
		rooms: () => {
			return {
				results: _(RoomService.currentRooms)
					.orderBy(["id"], ["asc"])
					.drop(0)
					.take(128)
					.value()
					.map(room => room.getInfo()),
				count: 0
			};
		},
		currentUser: (parent, args, { user }) => {
			if (user) {
				return {
					loggedIn: true,
					profile: _.pick(user, [
						"_id",
						"displayname",
						"pic",
						"lastlogin"
					])
				};
			}
			return {
				loggedIn: false
			};
		}
	},
	Mutation: {
		createRoom: (parent, { input }, { user }) => {
			// if (!user) return console.log("ACCESS DENIED");
			// else {
			const room = RoomService.createRoom(
				Object.assign(
					{
						media: [process.env.DEFAULT_ROOM_VIDEO_ID],
						cur: 0,
						owner: user ? user._id : ""
					},
					input
				)
			);
			return { id: room.id };
			// }
		}
	}
};
