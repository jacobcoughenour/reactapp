import fs from "fs";
import path from "path";
import Mustache from "mustache";
import { UserInfoModel } from "./Models";
import passport from "passport";
import { OAuth2Strategy } from "passport-google-oauth";
import RoomService from "./RoomService";

/**
 * Register api routes
 * @param {Express.Application} app
 */
export function register(app, clientpath) {
	// Passport auth
	passport.use(
		new OAuth2Strategy(
			{
				clientID: process.env.REACT_APP_OAUTH_CLIENT_ID,
				clientSecret: process.env.GOOGLE_CLIENT_SECRET,
				callbackURL:
					process.env.NODE_ENV === "development"
						? "http://localhost:8080/auth/google/callback"
						: "/auth/google/callback"
			},
			(accessToken, refreshToken, profile, cb) => {
				//console.log('Access Token:' +accessToken + '\n\n\n' + 'Refresh Token:' +refreshToken + '\n\n\n' + 'Profile:'+ profile + '\n\n\n' + cb)
				UserInfoModel.findByIdAndUpdate(
					profile.id,
					{
						_id: profile.id,
						displayname: profile.displayName,
						pic: profile.photos[0].value,
						lastLogin: Date.now()
					},
					{ upsert: true },
					err => {
						if (err) return cb(err);
						console.log(`User: #${profile.id} login successful`);
						cb(null, profile);
					}
				);
			}
		)
	);

	passport.serializeUser((user, cb) => {
		cb(null, user.id);
	});

	passport.deserializeUser((id, cb) => {
		UserInfoModel.findById(id, (err, res) => cb(err, res));
	});

	app.get("/logout", (req, res) => {
		// logout from passport
		req.logout();
		// redirect back
		res.redirect(req.header("Referer") || "/");
	});

	app.get(
		"/auth/google",
		(req, res, next) => {
			// store previous url in request session
			req.session.redirectTo = req.header("Referer");
			next();
		},
		passport.authenticate("google", {
			scope: "https://www.googleapis.com/auth/userinfo.profile"
		})
	);

	app.get(
		"/auth/google/callback",
		passport.authenticate("google", {}),
		(req, res) => {
			// redirect them back to where they came from
			res.redirect(req.session.redirectTo || "/");
			// make sure we clear this after redirecting
			delete req.session.redirectTo;
		}
	);

	// load index html file as a string
	const indexhtml = fs
		.readFileSync(path.join(clientpath, "index.html"), {
			enchoding: "utf-8"
		})
		.toString();

	// position to inject custom tags into the head
	const insertpos = indexhtml.match(/<head>/).index + 6;

	// template html for room meta
	const roomtemplate = `${indexhtml.slice(0, insertpos)}
	<link rel="alternate" type="application/json+oembed" href="/oembed.json?r={{oembed}}">
	<meta property="og:title" content="{{title}}">
	<meta property="og:description" content="{{description}}">
	<meta property="og:image" content="{{{thumb}}}">
	<meta property="og:type" content="video.other">
	<meta property="og:video:type" content="application/x-shockwave-flash">
	<meta property="og:url" content="{{{url}}}">
	${indexhtml.slice(insertpos)}`;
	Mustache.parse(roomtemplate);

	// handle room invite link meta tags
	// this injects the meta tags into the html for third-party apps to see when a link is shared
	app.get("/r/*", (req, res) => {
		res.set("Content-Type", "text/html");

		const id = req.path.slice(3);

		// if room does not exist
		if (!RoomService.currentRooms.hasOwnProperty(id)) {
			res.send(
				Mustache.render(roomtemplate, {
					title: "Vidr.tv",
					description: "invalid room link",
					thumb: "",
					url: "http://vidr.tv",
					oembed: ""
				})
			);
		} else {
			RoomService.currentRooms[id].getInfo().then(info => {
				res.send(
					Mustache.render(roomtemplate, {
						title: `Join ${info.nam || ""} on Vidr.tv`,
						description: `Now Playing: ${
							info.cur && info.cur.title ? info.cur.title : ""
						}`,
						thumb: info.cur ? info.cur.mediumThumb : "",
						url: `${req.protocol}://${req.get("host")}${
							req.originalUrl
						}`,
						oembed: encodeURIComponent(req.path.slice(3))
					})
				);
			});
		}
	});

	app.get("/oembed.json", [], (req, res) => {
		if (!RoomService.currentRooms.hasOwnProperty(req.query.r)) {
			res.json({
				title: "Vidr.tv",
				description: "invalid room link",
				version: "1.0",
				type: "rich",
				width: 480,
				height: 270,
				html: `<iframe width="480" height="270" src="${
					req.protocol
				}://${req.get("host")}/404" frameborder="0"></iframe>`
			});
		} else {
			RoomService.currentRooms[req.query.r].getInfo().then(info => {
				res.json({
					title: `Join ${info.name || ""} on Vidr.tv`,
					description: `Now Playing: ${
						info.cur && info.cur.title ? info.cur.title : ""
					}`,
					version: "1.0",
					type: "rich",
					width: 480,
					height: 270,
					html: `<iframe width="480" height="270" src="${
						req.protocol
					}://${req.get("host")}/embed/${
						req.query.r
					}" frameborder="0"></iframe>`
				});
			});
		}
	});

	app.get("/embed/*", (req, res) => {
		res.set("Content-Type", "text/html");
		res.send(indexhtml);
	});

	// catch 404s
	// redirects to home
	// TODO: display 404 message (redirect to /404 ?)
	app.get("/*", (req, res) => {
		res.redirect("/");
	});
}
