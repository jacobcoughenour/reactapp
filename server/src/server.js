import "dotenv/config";
import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import path from "path";
import chalk from "chalk";
import { status } from "./log";
import db from "./db";
import RoomService from "./RoomService";
import { register } from "./Routes.js";
import { ApolloServer } from "apollo-server-express";
import schema from "./graphql";
import passport from "passport";
import cookieParser from "cookie-parser";
import session from "express-session";
import connectMongo from "connect-mongo";

// Set up auto session storage in mongo
const mongoStore = connectMongo(session);

// Setup database

db.init();

// Setup Express server

console.log("[Express] Starting server");

const app = express();
const port = process.env.PORT || 8080;

// full path to this file as an array
let fullpath = path.dirname(__filename).split(path.sep);

// go to the project root
fullpath = fullpath.slice(0, fullpath.length - 2);

// path to client build dir
const clientpath = path.join(fullpath.join(path.sep), "client", "build");

// Serve the static files from the React app
app.use(express.static(clientpath));

// use cookie parser
app.use(cookieParser());

// body parser to read POSTs
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Set up session middleware
const sessionConfig = {
	key: "connect.sid",
	secret: String(process.env.SESSION_SECRET),
	resave: false,
	saveUninitialized: true,
	store: new mongoStore({
		mongooseConnection: mongoose.connection
	})
};

app.use(session(sessionConfig));
// passport middleware
app.use(passport.initialize());
app.use(passport.session());

// graphql apollo server
// Since passport initalizes and uses session, apollo will register it as well

const apollo = new ApolloServer({
	schema,
	context: ({ req }) => {
		return { user: req.user };
	}
});
apollo.applyMiddleware({ app });

const server = app.listen(port, () => {
	console.log(chalk.green("[Express] Listening on " + port));
	status.express = true;
});

// start room service
RoomService.start(sessionConfig, server);

// register routes
register(app, clientpath);
