import chalk from "chalk";
import { getMediaInfo } from "./Media";
import { RoomModel } from "./Models";
import _ from "lodash";

export default class Room {
	constructor(io, clock, doc) {
		this.io = io;
		this.clock = clock;
		this.id = doc._id;

		this.users = {};

		/** room db model */
		this.model = RoomModel(doc);
		this.log("loaded room");

		this.open = true;
		this.log("room is open for connections");

		if (this.model.media.length > 0) this.queuePlayback(0);
	}

	join(socket) {
		socket.join(this.model._id);

		if (socket.request.user.logged_in) {
			this.log(socket.request.user.displayname + " joined room");
			this.sendChatLogMessage(
				socket.request.user.displayname + " joined room"
			);

			this.users[socket.id] = _.pick(socket.request.user, [
				"_id",
				"displayname",
				"pic"
			]);
		} else {
			this.log("guest joined room");
			this.sendChatLogMessage("guest joined room");

			this.users[socket.id] = { _id: -1, displayname: "guest" };
		}

		// this.updateTime();
		socket.emit("fullsync", {
			name: this.model.name,
			media: this.model.media,
			cur: this.model.cur,
			start: this.model.start,
			users: this.users,
			owner: this.model.owner
		});

		this.io.to(this.id).emit("partialsync", {
			users: this.users
		});

		socket.on("clocksync", data => {
			// immediately respond
			socket.emit("clocksync", this.clock.processRequest(data));
			// do a time update and sync
			// this.updateTime();
			// this.io.to(this.id).emit("timesync", this.model.start);
		});

		socket.on("disconnect", () => {
			this.log(this.users[socket.id].displayname + " disconnected");
			this.sendChatLogMessage(
				this.users[socket.id].displayname + " left room"
			);
			socket.leave(this.model._id);

			delete this.users[socket.id];

			this.io.to(this.id).emit("partialsync", {
				users: this.users
			});
		});

		socket.on("addmedia", id => {
			if (socket.request.user)
				this.playlistAddMedia(id, this.users[socket.id].displayname);
		});

		socket.on("removemedia", id => {
			if (
				socket.request &&
				socket.request.user &&
				(!this.hasOwner() || this.isOwner(socket.request.user))
			)
				this.playlistRemoveMedia(id, this.users[socket.id].displayname);
		});

		socket.on("skip", () => {
			if (
				socket.request &&
				socket.request.user &&
				(!this.hasOwner() || this.isOwner(socket.request.user))
			)
				this.playlistSkip(this.users[socket.id].displayname);
		});

		socket.on("chatmessage", message => {
			if (socket.request.user) {
				this.io.to(this.id).emit("chatmessage", {
					type: "chat",
					from: this.users[socket.id],
					body: message
				});
			}
		});
	}

	playlistSkip(who = "server") {
		this.queuePlayback(
			(this.model.cur + 1) % this.model.media.length,
			1500
		);
		getMediaInfo("youtube", this.model.media[this.model.cur])
			.then(result => {
				this.sendChatLogMessage(`${who} skipped ${result.title}`);
			})
			.catch(error => {
				console.error(error);
			});
	}

	playlistAddMedia(id, who = "server") {
		if (!id || id.length !== 11) return;

		// media is already in the playlist
		if (this.model.media.indexOf(id) !== -1) return;

		// get media info to verify id
		getMediaInfo("youtube", id)
			.then(result => {
				const { cur, media } = this.model;

				// insert into array without changing index of cur
				if (cur === 0) {
					this.model.media.push(id);
				} else {
					// move first element to end
					// and insert id before cur
					this.model.media = [
						...media.slice(1, cur),
						id,
						...media.slice(cur),
						media[0]
					];
				}

				// send update to clients
				this._sendUpdatedPlaylist();

				this.sendChatLogMessage(
					`${who} added ${result.title} to the playlist`
				);
			})
			.catch(error => {
				console.error(error);
			});
	}

	playlistRemoveMedia(id, who = "server") {
		if (!id || id.length !== 11) return;

		// hold onto playlist
		const media = this.model.media;

		// get index of media in playlist
		const index = media.indexOf(id);

		// media is not in the playlist
		if (index === -1) return;

		// media is currently playing
		if (index === this.model.cur) return;

		// remove at the index
		if (this.model.cur < index) {
			this.model.media = [
				...media.slice(0, index),
				...media.slice(index + 1)
			];
		} else {
			this.model.media = [
				media[media.length - 1],
				...media.slice(0, index),
				...media.slice(index + 1, media.length - 1)
			];
		}

		// send updated playlist to clients
		this._sendUpdatedPlaylist();

		getMediaInfo("youtube", id)
			.then(result => {
				this.sendChatLogMessage(
					`${who} removed ${result.title} from the playlist`
				);
			})
			.catch(error => {
				console.error(error);
			});
	}

	playlistMoveMedia(id, direction) {
		if (!id || id.length !== 11) return;
		console.log(direction);
	}

	_sendUpdatedPlaylist() {
		this.io.to(this.id).emit("partialsync", {
			media: this.model.media
		});
	}

	save() {
		this.model.save();
		this.log("saved room to db");
	}

	_queuedTimeout = -1;

	queuePlayback(index, delay = 3000) {
		clearTimeout(this._queuedTimeout);

		this.model.cur = index;
		this.model.start = Date.now() + delay;

		getMediaInfo("youtube", this.model.media[this.model.cur])
			.then(result => {
				this.io.to(this.id).emit("fullsync", {
					name: this.model.name,
					media: this.model.media,
					cur: this.model.cur,
					duration: result.duration,
					start: this.model.start
				});

				this._queuedTimeout = setTimeout(() => {
					// start next video
					this.queuePlayback((index + 1) % this.model.media.length);
				}, result.duration + delay);
			})
			.catch(error => {
				console.error(error);

				this._queuedTimeout = setTimeout(() => {
					// start next video
					this.queuePlayback((index + 1) % this.model.media.length);
				}, delay);
			});
	}

	log(message) {
		console.log(`${chalk.cyan(`[Room<${this.model._id}>]`)} ${message}`);
	}

	sendChatLogMessage(message) {
		this.io.to(this.id).emit("chatmessage", {
			type: "log",
			body: message
		});
	}

	getOwner() {
		return this.model.owner;
	}

	hasOwner() {
		return this.model.owner !== "";
	}

	isOwner(user) {
		return user._id === this.getOwner();
	}

	getInfo() {
		return Promise.all(
			this.model.media.map(id => getMediaInfo("youtube", id))
		).then(result => {
			return {
				id: this.id,
				media: result,
				name: this.model.name,
				cur: result[this.model.cur],
				start: this.model.start,
				open: this.open,
				users: _.values(this.users)
			};
		});
	}
}
