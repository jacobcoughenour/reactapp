import { Component, Fragment } from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
// import { hot } from "react-hot-loader";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { withStyles, withTheme } from "@material-ui/core/styles";
import { Route, NavLink, BrowserRouter, Switch } from "react-router-dom";
import { Logo, ProfileArea } from "./components";
import { DefaultView, RoomView, CreateRoomView, JoinRoomView } from "./views";
import { UserContext, UserContextDefaultValue } from "./contexts";
import PropTypes from "prop-types";
import keymap from "./keymap.json";
import { ShortcutManager } from "react-shortcuts";
import "simplebar/dist/simplebar.min.css";
import "./App.css";

/**
 * Main content routes
 */
const ContentRoutes = () => (
	<Fragment>
		<Route exact path="/" component={DefaultView} />
		<Route path="/r/:id" component={RoomView} />
		<Route path="/join" component={JoinRoomView} />
		<Route path="/create" component={CreateRoomView} />
	</Fragment>
);

/**
 * Backgrounds for routes
 */
const backgrounds = (theme, location) => [
	{
		background: theme.palette.background.grad1,
		backgroundBlendMode: "multiply",
		opacity: location.pathname === "/" ? 0.8 : 0
	},
	{
		background: theme.palette.background.grad2,
		opacity: location.pathname === "/" ? 0 : 0.5
	},
	{
		background: theme.palette.background.grad3,
		opacity: location.pathname.lastIndexOf("/r/", 0) === 0 ? 1 : 0
	}
];

const userquery = gql`
	query {
		currentUser {
			loggedIn
			profile {
				_id
				displayname
				pic
				lastLogin
			}
		}
	}
`;

class App extends Component {
	shortcutManager = new ShortcutManager(keymap);

	static childContextTypes = {
		shortcuts: PropTypes.object.isRequired
	};

	getChildContext() {
		return { shortcuts: this.shortcutManager };
	}

	render() {
		return (
			<Query query={userquery}>
				{({ loading, error, data }) => (
					<UserContext.Provider
						value={Object.assign(
							UserContextDefaultValue,
							data.currentUser,
							{ loading }
						)}
					>
						<BrowserRouter>
							<Switch>
								{/* render embedded version */}
								<Route
									path="/embed/:id"
									render={({ ...props }) => (
										<RoomView embedded {...props} />
									)}
								/>
								{/* or render full app */}
								<Route component={FullApp} />
							</Switch>
						</BrowserRouter>
					</UserContext.Provider>
				)}
			</Query>
		);
	}
}

// add hot reloading in dev mode
// we have to wrap it withStyles for mobile breakpoints
export default withStyles({})(
	// process.env.NODE_ENV === "development" ? hot(module)(App) : App
	App
);

const FullApp = withTheme()(({ ...props }) => (
	<div
		className="App"
		css={{
			display: "flex",
			width: "100%",
			height: "100%",
			overflow: "hidden",
			flexDirection: "column",
			alignContent: "flex-start"
		}}
		style={{
			background: props.theme.palette.background.default
		}}
	>
		{backgrounds(props.theme, props.location).map((bg, key) => (
			<div
				key={key}
				css={{
					transition: "opacity 0.4s",
					position: "absolute",
					left: 0,
					right: 0,
					top: 0,
					bottom: 0,
					zIndex: 0
				}}
				style={bg}
			/>
		))}
		{props.location.pathname !== "/" && <Header />}
		<div
			className="content"
			css={{
				display: "flex",
				flex: 1,
				zIndex: 1
			}}
		>
			<ContentRoutes />
		</div>
	</div>
));

/**
 * App header bar
 */
const Header = () => (
	<header
		className="app-header"
		css={{
			background: "transparent",
			display: "flex",
			color: "white",
			flexShrink: 0,
			zIndex: 1,
			justifyContent: "center"
		}}
	>
		<div
			css={{
				display: "flex",
				padding: "8px 0",
				flex: 1,
				maxWidth: 1920
			}}
		>
			<NavLink
				to="/"
				css={{
					flex: 0,
					display: "inline-block",
					verticalAlign: "middle",
					textAlign: "center",
					height: 40,
					padding: "0 12px 0 28px",
					lineHeight: "38px",
					userSelect: "none",
					borderRadius: 4
				}}
			>
				<Logo />
			</NavLink>
			<div
				css={{
					flex: 1,
					display: "flex",
					justifyContent: "flex-end"
				}}
			>
				<ProfileArea />
			</div>
		</div>
	</header>
);
