import React from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import SimpleBar from "simplebar-react";
import { withTheme } from "@material-ui/core/styles";
import ButtonBase from "@material-ui/core/ButtonBase";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
// import MoreVertIcon from "@material-ui/icons/MoreVert";
import RemoveIcon from "@material-ui/icons/Clear";
import { Loader } from "../components";
import { views, formatTime } from "../utils";

export const MediaCard = ({ style, children, ...props }) => {
	const { id, cache, compact, actions, button, onClick } = props;

	const thumbsize = compact
		? {
				width: 106,
				height: 60
		  }
		: {
				width: 160,
				height: 90
		  };

	if (!cache.hasOwnProperty(id))
		return (
			<Card
				style={{
					padding: "4px 8px",
					borderRadius: 0,
					height: thumbsize.height + 8,
					width: "100%",
					boxSizing: "border-box",
					boxShadow: "none",
					textAlign: "center"
				}}
			>
				<Typography
					style={{
						padding: 0,
						overflow: "hidden",
						textOverflow: "ellipsis",
						maxHeight: "2.5rem"
					}}
					color="textSecondary"
				>
					Loading...
				</Typography>
			</Card>
		);

	return jsx(
		button ? ButtonBase : Card,
		{
			css: {
				display: "flex",
				flexDirection: "row",
				padding: "4px 8px",
				borderRadius: 0,
				width: "100%",
				boxSizing: "border-box",
				boxShadow: "none",
				textAlign: "left",
				":hover button.menu-icon": {
					opacity: 1
				}
			},
			style: {
				height: thumbsize.height + 8
			},
			onClick: onClick
		},
		<React.Fragment>
			<div
				style={{
					position: "relative",
					...thumbsize
				}}
			>
				<CardMedia
					style={thumbsize}
					image={
						cache[id].thumbnails && cache[id].thumbnails.medium.url
					}
					title={cache[id].title}
				/>
				<span
					style={{
						position: "absolute",
						bottom: 0,
						right: 0,
						background: "rgba(44,40,52,0.8)",
						borderRadius: 2,
						margin: 4,
						padding: "2px 4px",
						fontSize: 12,
						lineHeight: "12px",
						fontWeight: 500,
						color: "#FFFFFF",
						opacity: cache[id].durationSeconds ? 0.8 : 0
					}}
				>
					{cache[id].durationSeconds &&
						formatTime(cache[id].durationSeconds)}
				</span>
			</div>
			<div
				style={{
					flex: 1,
					padding: "4px 4px 4px 16px"
				}}
			>
				<CardHeader
					style={{
						padding: 0
					}}
					title={cache[id].title}
					titleTypographyProps={{
						variant: "body2",
						style: {
							display: "-webkit-box",
							padding: 0,
							width: "100%",
							overflow: "hidden",
							textOverflow: "ellipsis",
							whiteSpace: "normal",
							WebkitBoxOrient: "vertical",
							WebkitLineClamp: 2,
							maxHeight: 32,
							lineHeight: 1.15
						}
					}}
					subheader={cache[id].channel.title}
					subheaderTypographyProps={{
						variant: "caption"
					}}
					action={
						actions && (
							<React.Fragment>
								<IconButton
									className="menu-icon"
									css={{
										opacity: 0
									}}
									onClick={actions[0]}
								>
									<RemoveIcon />
								</IconButton>
							</React.Fragment>
						)
					}
				/>
				<CardContent
					style={{
						padding: 0
					}}
				>
					{!compact && (
						<Typography color="textSecondary" variant="caption">
							{views(cache[id].raw.statistics.viewCount)}
						</Typography>
					)}
				</CardContent>
			</div>
		</React.Fragment>
	);
};

@withTheme()
class PlaylistView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			data: {}
		};
	}

	handleRemoveClick = id => {
		this.props.roomview.socket.emit("removemedia", id);
	};

	render() {
		const { theme } = this.props;
		const { room, mediacache } = this.props.roomview.state;

		if (
			!room ||
			!room.media ||
			!room.media.every(e => mediacache.hasOwnProperty(e))
		)
			return (
				<div
					style={{
						width: "100%",
						height: "100%",
						position: "relative",
						// maxWidth: 500,
						backgroundColor: theme.palette.background.paper
					}}
				>
					<Loader />
				</div>
			);

		return (
			<div
				style={{
					width: "100%",
					height: "100%",
					position: "relative",
					backgroundColor: theme.palette.background.paper
				}}
			>
				<SimpleBar
					style={{
						position: "absolute",
						width: "100%",
						top: 0,
						left: 0,
						bottom: 0,
						overflowX: "hidden",
						display: "flex",
						flexDirection: "column"
					}}
				>
					{room.media
						.slice(room.cur)
						.concat(room.media.slice(0, room.cur))
						.map((id, index) => {
							return (
								<div key={index}>
									{index < 2 && (
										<Typography
											style={{
												padding: "8px 18px 0px 18px"
											}}
											variant="overline"
										>
											{index === 0
												? "Now Playing"
												: "Up Next"}
										</Typography>
									)}
									<MediaCard
										index={index}
										cache={mediacache}
										id={id}
										compact
										actions={
											index > 0 && [
												() => this.handleRemoveClick(id)
											]
										}
									/>
								</div>
							);
						})}
				</SimpleBar>
			</div>
		);
	}
}

export default PlaylistView;
