import { Component } from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import { MediaPlayer, Loader, AvatarGroup } from "../components";
import SimpleBar from "simplebar-react";
import { SidebarView } from "./";
import { withTheme } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import openSocket from "socket.io-client";
import { youtube } from "../youtube";
import { ClocksyClient } from "clocksy";
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowRightRounded";
import Fullscreen from "react-full-screen";
import { Shortcuts } from "react-shortcuts";

@withTheme()
class RoomView extends Component {
	constructor(props) {
		super(props);
		this.roomid = props.match.params.id;
		this.state = {
			room: {},
			mediacache: {},
			joining: true,
			error: false,
			loadingmessage: "Joining Room",
			fullscreen: false,
			theatermode: false,
			sidebaropen: false
		};

		this.clock = new ClocksyClient({
			sendRequest: req => this.socket.emit("clocksync", req),
			alpha: 0.15,
			updatePeriod: 3000
		});
	}

	componentDidMount() {
		document.title = "Vidr.tv - Joining...";

		document.head
			.querySelector(`meta[name="theme-color"]`)
			.setAttribute("content", this.props.theme.palette.background.paper);

		// open realtime socket connection
		this.socket = openSocket(
			process.env.NODE_ENV === "development"
				? window.location.hostname + ":8080"
				: null
		);

		this.socket.on("connect_error", error => {
			console.error(error);
			document.title = "Vidr.tv - Connection Error";
			this.setState({
				joining: true,
				error: true,
				loadingmessage: (
					<span>
						<div>Connection Error</div>
						<div>{error.toString()}</div>
					</span>
				)
			});
		});

		this.socket.on("connect_timeout", () => {
			console.error("connect_timeout");
			document.title = "Vidr.tv - Connection Timeout";
			this.setState({
				joining: false,
				error: true,
				loadingmessage: "Connection Timeout"
			});
		});

		this.socket.on("connect", () => {
			// join room by id
			this.socket.emit("join", { roomid: this.roomid });

			this.socket.on("joinerror", message => {
				console.error("joinerror", message);
				document.title = `Vidr.tv - ${message}`;
				this.setState({
					joining: true,
					error: true,
					loadingmessage: message
				});
			});

			// start clock
			this.clock.start();
			this.socket.on("clocksync", data => {
				this.clock.processResponse(data);
			});

			// first fullsync = joined room
			this.socket.on("fullsync", data => {
				console.log("fullsync", data);

				this.setState({
					room: Object.assign(this.state.room, data),
					joining: false
				});

				document.title = `Vidr.tv - ${data.name}`;

				let mediacache = this.state.mediacache;

				for (let i = 0; i < this.state.room.media.length; i++) {
					if (!mediacache.hasOwnProperty(this.state.room.media[i]))
						youtube
							.getVideoByID(this.state.room.media[i], {
								part: "snippet,contentDetails,statistics"
							})
							.then(result => {
								youtube
									.getChannelByID(result.channel.id, {
										part: "snippet"
									})
									.then(channel => {
										result.channel = channel;
										mediacache[
											this.state.room.media[i]
										] = result;

										this.setState({
											mediacache: mediacache
										});
									});
							})
							.catch(console.error);
				}
			});

			this.socket.on("partialsync", data => {
				console.log("partialsync", data);

				this.setState({ room: Object.assign(this.state.room, data) });

				if (data.hasOwnProperty("media")) {
					let mediacache = this.state.mediacache;

					for (let i = 0; i < this.state.room.media.length; i++) {
						if (
							!mediacache.hasOwnProperty(this.state.room.media[i])
						)
							youtube
								.getVideoByID(this.state.room.media[i], {
									part: "snippet,contentDetails,statistics"
								})
								.then(result => {
									youtube
										.getChannelByID(result.channel.id, {
											part: "snippet"
										})
										.then(channel => {
											result.channel = channel;
											mediacache[
												this.state.room.media[i]
											] = result;

											this.setState({
												mediacache: mediacache
											});
										});
								})
								.catch(console.error);
					}
				}
			});
		});

		this.socket.on("disconnect", () => this.clock.stop());

		this._isMounted = true;
	}

	componentWillUnmount() {
		// disconnect from socket when roomview unmounts
		this.socket.disconnect();

		this._isMounted = false;
	}

	toggleFullscreen = () => {
		this.setState({ fullscreen: !this.state.fullscreen });
	};

	toggleTheaterMode = () => {
		this.setState({ theatermode: !this.state.theatermode });
	};

	toggleSidebar = () => {
		this.setState({ sidebaropen: !this.state.sidebaropen });
	};

	openSidebar = () => {
		this.setState({ sidebaropen: true });
	};

	closeSidebar = () => {
		this.setState({ sidebaropen: false });
	};

	handleKeyboard = action => {
		if (action === "TOGGLE_FULLSCREEN") this.toggleFullscreen();
		else if (action === "TOGGLE_THEATERMODE") this.toggleTheaterMode();
	};

	render() {
		const {
			room,
			mediacache,
			joining,
			error,
			loadingmessage,
			fullscreen,
			sidebaropen
		} = this.state;

		if (joining) return <Loader error={error} message={loadingmessage} />;

		const { theme, embedded } = this.props;

		const cur = mediacache.hasOwnProperty(room.media[room.cur])
			? mediacache[room.media[room.cur]]
			: false;

		return (
			<Shortcuts
				name="ROOMVIEW"
				handler={this.handleKeyboard}
				targetNodeSelector="body"
				css={[
					{
						overflow: "hidden",
						width: "100%",
						boxSizing: "border-box",
						height: "100%",
						outlineStyle: "none",
						".fullscreen": {
							overflow: "hidden",
							display: embedded ? "block" : "flex",
							paddingBottom: embedded
								? 0
								: `${theme.spacing.unit}px`,
							maxWidth: embedded ? "none" : "1920px",
							margin: embedded ? 0 : `0 auto`,
							boxSizing: "border-box",
							width: "100%",
							height: "100%"
						}
					},
					!embedded && {
						[theme.breakpoints.down("xs")]: {
							".fullscreen": {
								paddingBottom: 0,
								margin: 0,
								overflowY: "scroll",
								WebkitOverflowScrolling: "touch",
								flexDirection: "column"
							}
						}
					}
				]}
			>
				<Fullscreen
					enabled={fullscreen}
					onChange={val => this.setState({ fullscreen: val })}
				>
					<main
						css={[
							{
								overflow: "hidden",
								display: "flex",
								flexDirection: "column"
							},
							!embedded && {
								[theme.breakpoints.up("sm")]: {
									flex: 2,
									margin: `0 ${theme.spacing.unit}px`,
									borderRadius: `0 0 ${theme.shape.borderRadius}px ${theme.shape.borderRadius}px`,
									"& > div": {
										height: "100%"
									}
								},
								[theme.breakpoints.down("xs")]: {
									flexShrink: 0
								}
							}
						]}
						style={
							embedded || fullscreen
								? {
										margin: 0
								  }
								: {}
						}
					>
						<SimpleBar
							style={{
								position:
									embedded || fullscreen
										? "absolute"
										: "relative",
								width: "100%",
								overflowX: "hidden",
								overflowY: embedded ? "hidden" : "auto",
								height:
									embedded || fullscreen ? "100%" : "inherit"
							}}
						>
							<div
								css={{
									width: "100%",
									flex: "0 1"
								}}
								style={{
									position:
										embedded || fullscreen
											? "absolute"
											: "relative"
								}}
							>
								<MediaPlayer
									className="player-container"
									// player height + heightdiff = player height breakpoint
									heightdiff={
										!embedded ? 148 + theme.spacing.unit : 0
									}
									socket={this.socket}
									clock={this.clock}
									room={room}
									roomview={this}
									mediacache={mediacache}
								/>
							</div>
							{!embedded && !fullscreen && (
								<div
									css={{
										backgroundColor:
											theme.palette.background.paper,
										padding: "12px 16px",
										[theme.breakpoints.up("sm")]: {
											borderRadius:
												theme.shape.borderRadius,
											marginTop: `${theme.spacing.unit}px`
										}
									}}
								>
									{cur ? (
										<div>
											<Typography
												variant="subtitle1"
												style={{
													color: "#FFF",
													fontSize: 18
												}}
											>
												{cur.title}
											</Typography>
											<div
												style={{
													borderBottom:
														"solid 1px rgba(255,255,255,0.1)",
													paddingBottom: "8px"
												}}
											>
												<Typography
													variant="subtitle1"
													style={{
														color: "#aaa",
														fontSize: 16
													}}
												>
													{parseInt(
														cur.raw.statistics
															.viewCount
													).toLocaleString()}
													{" views"}
												</Typography>
											</div>
											<div
												style={{
													margin: "12px 0",
													display: "flex"
												}}
											>
												<img
													alt={cur.channel.title}
													style={{
														borderRadius: "50%",
														height: 48,
														width: 48
													}}
													src={
														(!!cur.channel
															.thumbnails &&
															!!cur.channel
																.thumbnails
																.default &&
															cur.channel
																.thumbnails
																.default.url) ||
														""
													}
												/>
												<div
													style={{
														flex: 1,
														margin: "4px 12px"
													}}
												>
													<a
														target="_blank"
														rel="noopener noreferrer"
														href={cur.channel.url}
														style={{
															color: "#fff",
															opacity: 0.88,
															fontSize: 14,
															fontWeight: 500
														}}
													>
														{cur.channel.title}
													</a>
													<div
														style={{
															color: "#fff",
															opacity: 0.6,
															fontSize: 13,
															fontWeight: 400
														}}
													>
														{"Published on "}
														{cur.publishedAt.toDateString()}
													</div>
												</div>
											</div>
										</div>
									) : (
										<div>Loading...</div>
									)}
								</div>
							)}
						</SimpleBar>
					</main>
					<div
						css={{
							display: "flex",
							flexDirection: "column",
							flex: 1,
							[theme.breakpoints.up("sm")]: {
								maxWidth: 380,
								minWidth: 320,
								marginRight: `${theme.spacing.unit}px`,
								borderRadius: theme.shape.borderRadius
							},
							[theme.breakpoints.down("xs")]: {
								minHeight: 480
							}
						}}
						style={{
							position:
								embedded || fullscreen
									? "absolute"
									: "relative",
							boxSizing: "border-box",
							top: embedded || fullscreen ? "50%" : "",
							transform:
								embedded || fullscreen
									? "translateY(-50%)"
									: "",
							left:
								embedded || fullscreen
									? sidebaropen
										? "unset"
										: "100%"
									: "unset",
							right:
								embedded || fullscreen
									? sidebaropen
										? "0%"
										: "unset"
									: "unset",
							height: embedded || fullscreen ? "100%" : "",
							maxHeight: embedded || fullscreen ? 720 : "",
							minWidth: embedded || fullscreen ? 320 : "",
							marginRight: embedded || fullscreen ? 0 : "",
							boxShadow:
								sidebaropen && (embedded || fullscreen)
									? theme.shadows[8]
									: "none",
							transition: "box-shadow 0.4s, left 0.4s, right 0.4s"
						}}
					>
						<IconButton
							onClick={this.closeSidebar}
							style={{
								display:
									sidebaropen && (embedded || fullscreen)
										? "inherit"
										: "none",
								background: "rgba(34, 31, 40, 0.15)",
								position: "absolute",
								top: "50%",
								left: -8,
								transform: "translate(-100%, -50%)",
								opacity: sidebaropen ? 1 : 0,
								transition: "opacity 0.4s"
							}}
						>
							<ArrowRightIcon />
						</IconButton>
						<div
							style={{
								background: theme.palette.background.paper,
								padding: "12px 18px",
								display: "flex"
							}}
						>
							<Typography variant="body1" style={{ flex: 1 }}>
								{room.name}
							</Typography>
							<AvatarGroup users={Object.values(room.users)} />
						</div>
						<SidebarView
							style={{
								display: "flex",
								flexDirection: "column",
								position: "relative",
								backgroundColor: theme.palette.background.paper,
								overflow: "hidden",
								borderRadius: `${theme.shape.borderRadius}`,
								flex: 1
							}}
							roomview={this}
						/>
					</div>
				</Fullscreen>
			</Shortcuts>
		);
	}
}

export default RoomView;
