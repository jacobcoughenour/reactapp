import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import SimpleBar from "simplebar-react";
import { Loader, RoomCard } from "../components";

const query = gql`
	query RoomListPage($filter: String!) {
		rooms(filter: $filter) {
			results {
				id
				name
				cur {
					source
					id
					title
					mediumThumb
				}
				users {
					displayname
					pic
				}
			}
		}
	}
`;

class JoinRoomView extends React.Component {
	componentDidMount() {
		document.title = "Vidr.tv - Join a Room";
		document.head
			.querySelector(`meta[name="theme-color"]`)
			.setAttribute("content", "#263245");
	}

	render() {
		return (
			<Query query={query} variables={{ filter: "" }}>
				{({ loading, error, data }) =>
					loading || error ? (
						<Loader error={error} message="Loading Rooms" />
					) : (
						<SimpleBar
							style={{
								width: "100%",
								height: "100%",
								overflowX: "hidden"
							}}
						>
							<div
								style={{
									display: "flex",
									marginTop: 8,
									flexDirection: "column",
									alignItems: "center",
									width: "100%"
								}}
							>
								{data.rooms.results.map(room => (
									<RoomCard room={room} key={room.id} />
								))}
							</div>
						</SimpleBar>
					)
				}
			</Query>
		);
	}
}

export default JoinRoomView;
