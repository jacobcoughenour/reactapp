import React from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Redirect } from "react-router";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import { withTheme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import Switch from "@material-ui/core/Switch";
import { Loader } from "../components";

const mutation = gql`
	mutation CreateRoom($input: RoomSettings) {
		createRoom(input: $input) {
			id
			errors
		}
	}
`;

@withTheme()
class CreateRoomView extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			activeStep: 0,
			data: {
				name: "My Room",
				public: true
			}
		};
	}

	componentDidMount() {
		document.title = "Vidr.tv - Create a Room";
	}

	handleNext = () => {
		const { activeStep } = this.state;
		this.setState({
			activeStep: activeStep + 1
		});
	};

	handleBack = () => {
		this.setState(state => ({
			activeStep: state.activeStep - 1
		}));
	};

	handleChange = (name, key) => event => {
		const data = this.state.data;
		data[name] = event.target[key];
		this.setState({ data });
	};

	render() {
		const { theme } = this.props;
		const { activeStep } = this.state;

		const steps = {
			Details: (
				<form
					onSubmit={event => {
						event.preventDefault();
					}}
					style={{}}
				>
					<FormGroup row>
						<FormControl>
							<InputLabel htmlFor="name-input">
								Room Name
							</InputLabel>
							<Input
								id="name-input"
								aria-describedby="name-helper-text"
								value={this.state.data.name}
								onChange={this.handleChange("name", "value")}
							/>
							<FormHelperText id="name-helper-text">
								The display name for your room
							</FormHelperText>
						</FormControl>
					</FormGroup>
					<FormGroup row>
						<FormControlLabel
							control={
								<Switch
									checked={this.state.data.public}
									onChange={this.handleChange(
										"public",
										"checked"
									)}
									value="public"
									color="primary"
								/>
							}
							label="Public"
						/>
					</FormGroup>
				</form>
			),
			Playlist: "Add at least one video to the playlist",
			Share: "share the url for other users to join your room"
		};

		const steplabels = Object.keys(steps);

		return (
			<main
				className="create-room"
				style={{
					position: "absolute",
					top: "50%",
					left: "50%",
					transform: "translate(-50%, -50%)",
					width: "100%",
					maxWidth: 400
				}}
			>
				<Mutation mutation={mutation}>
					{(CreateRoom, { loading, error, data }) => {
						if (loading || error)
							return (
								<Loader error={error} message="Creating Room" />
							);
						if (data)
							return <Redirect to={`/r/${data.createRoom.id}`} />;
						return (
							<Paper
								style={{
									padding: theme.spacing.unit * 3
								}}
							>
								<Typography variant="h4" align="center">
									Create a Room
								</Typography>
								<Stepper activeStep={activeStep}>
									{steplabels.map((label, key) => (
										<Step key={key}>
											<StepLabel>{label}</StepLabel>
										</Step>
									))}
								</Stepper>
								<div
									style={{
										margin: `0 ${theme.spacing.unit * 2}px`
									}}
								>
									<Typography variant="h6">
										{steplabels[activeStep]}
									</Typography>
									{steps[steplabels[activeStep]]}
								</div>
								<div
									style={{
										display: "flex",
										justifyContent: "flex-end",
										marginTop: theme.spacing.unit * 3
									}}
								>
									<Button
										disabled={activeStep === 0}
										onClick={this.handleBack}
									>
										Back
									</Button>
									<Button
										variant="contained"
										color={
											activeStep === steplabels.length - 1
												? "secondary"
												: "primary"
										}
										onClick={
											activeStep === steplabels.length - 1
												? () =>
														CreateRoom({
															variables: {
																input: this
																	.state.data
															}
														})
												: this.handleNext
										}
										style={{
											marginLeft: theme.spacing.unit
										}}
									>
										{activeStep === steplabels.length - 1
											? "Finish"
											: "Next"}
									</Button>
								</div>
							</Paper>
						);
					}}
				</Mutation>
			</main>
		);
	}
}

export default CreateRoomView;
