import React from "react";
import SimpleBar from "simplebar-react";
import { withTheme } from "@material-ui/core/styles";
import { UserAvatar } from "../components";

@withTheme()
class ChatView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			messages: []
		};
	}

	_isMounted = false;

	componentDidMount() {
		this._isMounted = true;
		this.props.roomview.socket.on("chatmessage", message => {
			if (this._isMounted) {
				this.setState({
					messages: [...this.state.messages, message]
				});
			}
		});
	}

	componentWillUnmount() {
		this._isMounted = false;
	}

	componentDidUpdate() {
		if (this._isMounted) this.scrollToBottom();
	}

	scrollToBottom = () => {
		if (this._isMounted) this.messagesEnd.scrollIntoView(false);
	};

	render() {
		const { theme } = this.props;

		return (
			<div
				style={{
					width: "100%",
					height: "100%",
					position: "relative",
					backgroundColor: theme.palette.background.paper
				}}
			>
				<div
					style={{
						position: "absolute",
						top: 0,
						bottom: 86,
						left: 0,
						right: 0
					}}
				>
					<SimpleBar
						style={{
							position: "relative",
							height: "100%",
							width: "100%",
							overflowX: "hidden"
						}}
					>
						<div
							style={{
								padding: "20px 0px 2px 0px"
							}}
						>
							{this.state.messages.map((message, i) =>
								message.type === "chat" ? (
									<div
										key={i}
										style={{
											marginTop: 2,
											padding: "2px 8px",
											display: "flex"
										}}
									>
										<UserAvatar
											user={message.from}
											size={32}
											style={{
												margin: "0 8px 0 2px"
											}}
										/>
										<div
											style={{
												background:
													"rgba(255,255,255,0.03)",
												color:
													theme.palette.text.primary,
												borderRadius: 10,
												fontSize: 14,
												margin: "2px 20px 0 0",
												padding: "6px 12px 6px 12px"
											}}
										>
											{message.body}
										</div>
									</div>
								) : (
									<div
										key={i}
										style={{
											marginTop: 2,
											padding: "2px 8px",
											display: "flex"
										}}
									>
										<div
											style={{
												color:
													theme.palette.text
														.secondary,
												borderRadius: 10,
												textAlign: "center",
												fontSize: 12,
												padding: "6px 12px 6px 12px"
											}}
										>
											{message.body}
										</div>
									</div>
								)
							)}
							<div
								style={{ float: "left", clear: "both" }}
								ref={el => {
									this.messagesEnd = el;
								}}
							/>
						</div>
					</SimpleBar>
				</div>
			</div>
		);
	}
}

export default ChatView;
