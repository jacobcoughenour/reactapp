import React from "react";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import Switch from "@material-ui/core/Switch";
import "whatwg-fetch";

export default class RoomSettingsView extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: props.data
		};
	}

	handleChange = (name, key) => event => {
		let data = this.state.data;
		data[name] = event.target[key];
		this.setState({ data });
	};

	render() {
		const { onSubmit } = this.props;
		return (
			<form
				onSubmit={event => {
					event.preventDefault();
					onSubmit(this.state.data);
				}}
				style={this.props.style}
			>
				<FormGroup row>
					<FormControl>
						<InputLabel htmlFor="name-input">Room Name</InputLabel>
						<Input
							id="name-input"
							aria-describedby="name-helper-text"
							value={this.state.data.name}
							onChange={this.handleChange("name", "value")}
						/>
						<FormHelperText id="name-helper-text">
							The display name for your room
						</FormHelperText>
					</FormControl>
				</FormGroup>
				<FormGroup row>
					<FormControlLabel
						control={
							<Switch
								checked={this.state.data.public}
								onChange={this.handleChange(
									"public",
									"checked"
								)}
								value="public"
								color="primary"
							/>
						}
						label="Public"
					/>
				</FormGroup>
				{/* <FormGroup row>
					<Button type="submit">Submit</Button>
				</FormGroup> */}
			</form>
		);
	}
}
