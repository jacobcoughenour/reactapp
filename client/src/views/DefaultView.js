import React from "react";
import { NavLink } from "react-router-dom";
import { Logo } from "../components";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

export default class DefaultView extends React.Component {
	componentDidMount() {
		document.title = "Vidr.tv";
		document.head
			.querySelector(`meta[name="theme-color"]`)
			.setAttribute("content", "#311b92");
	}

	render() {
		return (
			<div
				className="center"
				style={{
					position: "absolute",
					display: "flex",
					flexDirection: "column",
					justifyContent: "center",
					alignItems: "center",
					textAlign: "center",
					left: 0,
					right: 0,
					top: 0,
					bottom: 0
				}}
			>
				<Logo full />
				<Typography
					variant="h5"
					style={{ margin: "0px 12px 28px 12px", maxWidth: 600 }}
				>
					{"Experience Together"}
				</Typography>
				<div>
					<Button
						style={{ marginRight: 16 }}
						variant="contained"
						color="secondary"
						component={NavLink}
						to="/join"
					>
						JOIN ROOM
					</Button>
					<Button
						variant="contained"
						color="secondary"
						component={NavLink}
						to="/create"
					>
						CREATE ROOM
					</Button>
				</div>
			</div>
		);
	}
}
