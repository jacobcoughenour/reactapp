import { Component } from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import { withTheme } from "@material-ui/core/styles";
import withMobileDialog from "@material-ui/core/withMobileDialog";
import SwipeableViews from "react-swipeable-views";
import AppBar from "@material-ui/core/AppBar";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Zoom from "@material-ui/core/Zoom";
import Slide from "@material-ui/core/Slide";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import SendIcon from "@material-ui/icons/Send";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { ChatView, PlaylistView } from "./";
import { MediaCard } from "./PlaylistView";
import { youtube } from "../youtube";

@withMobileDialog({ breakpoint: "xs" })
@withTheme()
class SidebarView extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: 1,
			input: "",
			searchinput: "",
			searchresults: [],
			related: [],
			searchcache: {},
			addDialogOpen: false
		};
	}

	handleChange = (event, value) => {
		this.setState({ value });
	};

	handleInputChange = event => {
		this.setState({ input: event.target.value });
	};
	handleSearchInputChange = event => {
		this.setState({ searchinput: event.target.value });
	};

	handleChangeIndex = index => {
		this.setState({ value: index });
	};

	onSubmit = event => {
		event.preventDefault();
		this.sendChatMessage();
	};

	sendChatMessage() {
		if (this.state.input.length === 0) return;

		// do not update the chat view from here
		this.props.roomview.socket.emit("chatmessage", this.state.input);

		this.setState({ input: "" });
	}

	handleAddClick = () => {
		this.search(true);
		this.setState({
			addDialogOpen: true,
			searchinput: "",
			searchresults: []
		});
	};

	handleAddDialogClose = () => {
		this.setState({ addDialogOpen: false });
	};

	handleAddDialogSearchSubmit = event => {
		event.preventDefault();
		this.search(false);
	};

	search = related => {
		const room = this.props.roomview.state.room;
		const searchcache = this.state.searchcache;

		youtube.request
			.getPaginated(
				"search",
				20,
				Object.assign(
					{
						type: "video",
						part: "snippet"
					},
					related
						? {
								relatedToVideoId: room.media[room.cur]
						  }
						: {
								q: this.state.searchinput
						  }
				)
			)
			.then(results => {
				results.forEach(result => {
					// todo this doesn't handle related videos anymore.

					searchcache[result.id.videoId] = {
						id: result.id.videoId,
						title: result.snippet.title,
						channel: {
							title: result.snippet.channelTitle
						},
						thumbnails: result.snippet.thumbnails
					};
				});

				this.setState({
					searchcache,
					[related ? "related" : "searchresults"]: results.map(
						vid => vid.id.videoId
					)
				});
			})
			.catch(error => {
				console.error(error);
			});
	};

	handleAddDialogSelection = id => {
		this.props.roomview.socket.emit("addmedia", id);
		this.handleAddDialogClose();
	};

	render() {
		const { theme, style, roomview } = this.props;
		const { searchresults, searchinput, searchcache, related } = this.state;

		const transitionDuration = {
			enter: theme.transitions.duration.enteringScreen,
			exit: theme.transitions.duration.leavingScreen
		};

		const pages = [
			{
				name: "Chat",
				content: <ChatView roomview={roomview} />,
				fab: {
					color: "primary",
					icon: <SendIcon />,
					click: () => this.sendChatMessage()
				}
			},
			{
				name: "Playlist",
				content: <PlaylistView roomview={roomview} />,
				fab: {
					color: "secondary",
					icon: (
						<AddIcon
							style={{
								transition: `transform ${
									this.state.addDialogOpen
										? transitionDuration.enter
										: transitionDuration.exit
								}ms ${theme.transitions.easing.easeInOut} 0ms`,
								transform: this.state.addDialogOpen
									? "rotate(135deg)"
									: "rotate(0deg)"
							}}
						/>
					),
					click: this.handleAddClick
				}
			}
		];

		return (
			<div css={style}>
				<AppBar position="static" color="default">
					<Tabs
						value={this.state.value}
						onChange={this.handleChange}
						indicatorColor="primary"
						textColor="primary"
						variant="fullWidth"
					>
						{pages.map((page, index) => (
							<Tab label={page.name} key={index} />
						))}
					</Tabs>
				</AppBar>
				<div
					style={{
						position: "relative",
						flex: 1,
						display: "flex",
						flexDirection: "column"
					}}
				>
					<SwipeableViews
						axis={theme.direction === "rtl" ? "x-reverse" : "x"}
						index={this.state.value}
						onChangeIndex={this.handleChangeIndex}
						containerStyle={{ height: "100%" }}
						style={{ flex: 1, overflow: "hidden" }}
						slideStyle={{
							height: "100%",
							overflow: "hidden"
						}}
					>
						{pages.map((page, index) => (
							<span key={index}>{page.content}</span>
						))}
					</SwipeableViews>
					<div
						style={{
							position: "absolute",
							bottom: 0,
							left: 0,
							right: 0,
							pointerEvents:
								this.state.value === 0 ? "auto" : "none"
						}}
					>
						<Slide
							direction="up"
							in={this.state.value === 0}
							timeout={transitionDuration}
							style={{
								transitionDelay: `${
									this.state.value === 0
										? transitionDuration.exit
										: 0
								}ms`,
								opacity: this.state.value === 0 ? 1 : 0
							}}
						>
							<form
								style={{
									flex: 0,
									padding: `${theme.spacing.unit}px ${theme
										.spacing.unit * 12}px ${
										theme.spacing.unit
									}px ${theme.spacing.unit}px`
								}}
								noValidate
								autoComplete="off"
								onSubmit={this.onSubmit}
							>
								<TextField
									id="outlined-full-width"
									variant="outlined"
									label="Message"
									style={{
										margin: "8px 12px 6px 8px"
									}}
									value={this.state.input}
									onChange={this.handleInputChange}
									margin="normal"
									fullWidth
								/>
							</form>
						</Slide>
						{pages.map((page, index) => (
							<Zoom
								key={page.fab.color}
								in={this.state.value === index}
								timeout={transitionDuration}
								style={{
									transitionDelay: `${
										this.state.value === index
											? transitionDuration.exit
											: 0
									}ms`,
									pointerEvents: "auto"
								}}
								unmountOnExit
							>
								<Fab
									style={{
										position: "absolute",
										bottom: theme.spacing.unit * 2,
										right: theme.spacing.unit * 2
									}}
									disabled={
										page.name === "Chat" &&
										this.state.input.length === 0
									}
									color={page.fab.color}
									onClick={page.fab.click}
								>
									{page.fab.icon}
								</Fab>
							</Zoom>
						))}
					</div>
				</div>
				<Dialog
					fullScreen={this.props.fullScreen}
					open={this.state.addDialogOpen}
					onClose={this.handleAddDialogClose}
					aria-labelledby="playlist-add-dialog-title"
					maxWidth="sm"
					scroll="paper"
				>
					<DialogTitle id="playlist-add-dialog-title">
						Add a Video to the Playlist
					</DialogTitle>
					<DialogTitle
						disableTypography
						style={{
							padding: "0 24px 8px 24px"
						}}
					>
						<Paper
							style={{
								background: "rgba(0,0,0,0.2)",
								boxShadow: "none"
							}}
						>
							<form
								id="playlist-add-dialog-search-form"
								style={{
									display: "flex"
								}}
								noValidate
								autoComplete="off"
								onSubmit={this.handleAddDialogSearchSubmit}
							>
								<TextField
									autoFocus
									id="playlist-add-dialog-search-input"
									style={{
										paddingLeft: 18,
										paddingTop: 8,
										flex: 1
									}}
									placeholder="Search YouTube"
									margin="none"
									value={searchinput}
									onChange={this.handleSearchInputChange}
								/>
								<IconButton
									aria-label="Search"
									type="submit"
									form="playlist-add-dialog-search-form"
								>
									<SearchIcon />
								</IconButton>
							</form>
						</Paper>
						{searchresults.length === 0 && (
							<div
								style={{
									width: 420,
									paddingTop: 8
								}}
							>
								<Typography
									style={{
										padding: "0px 18px"
									}}
									variant="overline"
								>
									Related Videos
								</Typography>
							</div>
						)}
					</DialogTitle>
					<DialogContent
						style={{
							maxWidth: 420
						}}
					>
						{(searchresults.length === 0
							? related
							: searchresults
						).map((id, index) => (
							<MediaCard
								button
								compact
								onClick={() =>
									this.handleAddDialogSelection(id)
								}
								index={index}
								cache={searchcache}
								id={id}
								key={index}
							/>
						))}
					</DialogContent>
				</Dialog>
			</div>
		);
	}
}

export default SidebarView;
