export function pluralize(amount, units, short = false, none = "") {
	return `${
		none.length > 0 && amount === 0
			? none
			: short
			? abbreviate(amount)
			: amount
	} ${units}${amount === 1 ? "" : "s"}`;
}

export function abbreviate(number) {
	var exp = Number(number)
		.toExponential()
		.split("e+")
		.map(el => +el);

	var mod = exp[1] % 3;

	return `${Math.round(exp[0] * Math.pow(10, mod + 1)) / 10}${
		["", "K", "M", "B", "T"][(exp[1] - mod) / 3]
	}`;
}

export function views(amount) {
	return pluralize(amount, "view", true, "No");
}

export function formatTime(current = 0) {
	let h = Math.floor(current / 3600);
	let m = Math.floor((current - h * 3600) / 60);
	let s = Math.floor(current % 60);
	return h > 0
		? h + ":" + (m < 10 ? `0${m}` : m) + ":" + (s < 10 ? `0${s}` : s)
		: m + ":" + (s < 10 ? `0${s}` : s);
}
