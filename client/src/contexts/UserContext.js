import { createContext } from "react";

export const defaultValue = {
	loading: true,
	loggedIn: false,
	profile: {},
	signOut: () => {
		console.log("signout?");
	}
};

export default createContext(defaultValue);
