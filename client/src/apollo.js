import ApolloClient, { InMemoryCache } from "apollo-boost";

export default new ApolloClient({
	uri: "/graphql",
	cache: new InMemoryCache()
});
