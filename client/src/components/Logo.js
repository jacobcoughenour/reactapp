import { Component } from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";

export default class Logo extends Component {
	render() {
		const { full } = this.props;
		return (
			<div
				className="logo"
				style={{
					color: "#FFF",
					userSelect: "none",
					fontSize: full ? 64 : 24
				}}
			>
				<span style={{ fontSize: full ? 96 : 24 }}>Vidr</span>
				.tv
				{!full && (
					<span
						style={{
							paddingLeft: 6,
							fontSize: 12,
							opacity: 0.6,
							fontFamily: "Roboto"
						}}
					>
						alpha
					</span>
				)}
			</div>
		);
	}
}
