import { Component } from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { MediaCard } from "../views/PlaylistView";
import { youtube } from "../youtube";

class MediaSearch extends Component {
	constructor(props) {
		super(props);

		this.state = {
			searchinput: "",
			searchresults: [],
			related: [],
			searchcache: {}
		};
	}

	handleSearchInputChange = event => {
		this.setState({ searchinput: event.target.value });
	};

	handleAddClick = () => {
		this.setState({
			addDialogOpen: true,
			searchinput: "",
			searchresults: []
		});
	};

	handleAddDialogClose = () => {
		this.setState({ addDialogOpen: false });
	};

	handleAddDialogSearchSubmit = event => {
		event.preventDefault();
		this.search(false);
	};

	search = related => {
		const searchcache = this.state.searchcache;

		youtube.request
			.getPaginated(
				"search",
				20,
				Object.assign(
					{
						type: "video",
						part: "snippet"
					},
					{
						q: this.state.searchinput
					}
				)
			)
			.then(results => {
				results.forEach(result => {
					searchcache[result.id.videoId] = {
						id: result.id.videoId,
						title: result.snippet.title,
						channel: {
							title: result.snippet.channelTitle
						},
						thumbnails: result.snippet.thumbnails
					};
				});

				this.setState({
					searchcache,
					[related ? "related" : "searchresults"]: results.map(
						vid => vid.id.videoId
					)
				});
			});
	};

	handleAddDialogSelection = id => {
		// this.props.roomview.socket.emit("addmedia", id);
		this.handleAddDialogClose();
	};

	render() {
		const { style } = this.props;
		const { searchresults, searchinput, searchcache, related } = this.state;

		return (
			<div css={style}>
				<div
					style={{
						padding: "0 0 8px 0",
						background: "red"
					}}
				>
					<Paper
						style={{
							background: "rgba(0,0,0,0.2)",
							boxShadow: "none"
						}}
					>
						<form
							id="playlist-add-dialog-search-form"
							style={{
								display: "flex"
							}}
							noValidate
							autoComplete="off"
							onSubmit={this.handleAddDialogSearchSubmit}
						>
							<TextField
								autoFocus
								id="playlist-add-dialog-search-input"
								style={{
									paddingLeft: 18,
									paddingTop: 8,
									flex: 1
								}}
								placeholder="Search YouTube"
								margin="none"
								value={searchinput}
								onChange={this.handleSearchInputChange}
							/>
							<IconButton
								aria-label="Search"
								type="submit"
								form="playlist-add-dialog-search-form"
							>
								<SearchIcon />
							</IconButton>
						</form>
					</Paper>
					{searchresults.length === 0 && (
						<div
							style={{
								width: 420,
								paddingTop: 8
							}}
						>
							<Typography
								style={{
									padding: "0px 18px"
								}}
								variant="overline"
							>
								Related Videos
							</Typography>
						</div>
					)}
				</div>
				<div
					style={{
						maxWidth: 420
					}}
				>
					{(searchresults.length === 0 ? related : searchresults).map(
						(id, index) => (
							<MediaCard
								button
								compact
								onClick={() =>
									this.handleAddDialogSelection(id)
								}
								index={index}
								cache={searchcache}
								id={id}
								key={index}
							/>
						)
					)}
				</div>
			</div>
		);
	}
}

export default MediaSearch;
