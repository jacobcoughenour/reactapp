import React from "react";
import Avatar from "@material-ui/core/Avatar";
import PersonIcon from "@material-ui/icons/Person";
import Tooltip from "@material-ui/core/Tooltip";

export default ({ ...props }) => {
	const { users, style } = props;

	return (
		<div
			style={{
				display: "inline-flex",
				...style
			}}
		>
			{users.map((user, key) => (
				<UserAvatar
					key={key}
					user={user}
					style={{
						marginRight: -6
					}}
				/>
			))}
		</div>
	);
};

const UserAvatar = ({ ...props }) => {
	const icon = (
		<Avatar
			style={{
				height: props.size || 24,
				width: props.size || 24,
				...props.style
			}}
			alt={props.user.displayname}
			src={props.user.pic}
		>
			{!props.user.pic && <PersonIcon fontSize="small" />}
		</Avatar>
	);

	return props.user.displayname ? (
		<Tooltip title={props.user.displayname}>{icon}</Tooltip>
	) : (
		icon
	);
};

export { UserAvatar };
