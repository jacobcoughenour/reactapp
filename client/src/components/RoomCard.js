/** @jsx jsx */
import { jsx } from "@emotion/core";
import { withTheme } from "@material-ui/core/styles";
import ButtonBase from "@material-ui/core/ButtonBase";
import { NavLink } from "react-router-dom";
import { AvatarGroup } from "./";

export default withTheme()(({ ...props }) => (
	<ButtonBase
		component={NavLink}
		to={`/r/${props.room.id}`}
		css={{
			background: "rgba(170,0,255 ,1)",
			color: "rgba(255, 255, 255, 0.8)",
			position: "relative",
			transition: `all ${props.theme.transitions.duration.standard}ms`,
			transitionTimingFunction: props.theme.transitions.easing.easeOut,
			overflow: "hidden",
			textAlign: "left",
			borderRadius: props.theme.shape.borderRadius,
			boxShadow: props.theme.shadows[2],
			"&:hover": {
				boxShadow: props.theme.shadows[4],
				color: "rgba(255, 255, 255, 0.9)",
				"img.thumb": {
					opacity: 1,
					transform: "scale(1.01)"
				}
			},
			width: 480,
			height: 180,
			margin: 8,
			[props.theme.breakpoints.down("xs")]: {
				boxShadow: "none",
				"&:hover": {
					boxShadow: "none"
				},
				width: "100%",
				height: 196,
				margin: 0,
				borderRadius: 0
			}
		}}
		{...props}
	>
		<img
			className="thumb"
			src={props.room.cur.mediumThumb}
			alt="now playing thumb"
			css={{
				opacity: 0.8,
				transition: `opacity ${
					props.theme.transitions.duration.standard
				}ms, transform ${props.theme.transitions.duration.complex}ms`,
				transitionTimingFunction:
					props.theme.transitions.easing.easeInOut,
				position: "absolute",
				right: 0,
				width: "auto",
				height: "100%"
			}}
		/>
		<div
			style={{
				position: "absolute",
				width: "100%",
				height: "100%",
				background:
					"linear-gradient(100deg, rgba(124,77,255 ,1) 40%, rgba(170,0,255 ,.1) 100%)"
			}}
		/>
		<div
			className="room-item-info"
			style={{
				display: "flex",
				flexDirection: "column",
				position: "absolute",
				width: "100%",
				height: "100%"
			}}
		>
			<div
				style={{
					flex: 1,
					display: "inline-flex",
					flexDirection: "column",
					padding: "12px 12px"
				}}
			>
				<h3 style={{ fontSize: 24, margin: 12 }}>{props.room.name}</h3>
				<p
					style={{
						fontSize: 14,
						margin: 12,
						flex: 1,
						opacity: 0.8
					}}
				>
					Now Playing <span>{props.room.cur.title}</span>
				</p>
			</div>
			<div
				style={{
					// background: 'rgba(16,16,16,0.1)',
					padding: 12,
					display: "flex"
				}}
			>
				<AvatarGroup
					style={{
						display: "flex",
						flex: 1,
						margin: 6
					}}
					users={props.room.users}
				/>
			</div>
		</div>
	</ButtonBase>
));
