import React from "react";
import { Global, css } from "@emotion/core";

export default class StatsOverlay extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			info: {},
			events: {}
		};

		this._updated = [];
		this._eventTimeout = -1;

		props.player._statsOverlay = this;
	}

	componentDidMount() {
		this._isMounted = true;
	}

	componentWillUnmount() {
		this._isMounted = false;
		clearTimeout(this._eventTimeout);
	}

	color(input) {
		if (input === "yellow" || input === "warn") return "#FFEE58";
		if (input === "red" || input === "error" || input === "bad")
			return "#F50057";
		if (input === "green" || input === "good") return "#76FF03";
		if (input === "blue" || input === "event") return "#00E5FF";

		return input;
	}

	set(name, value, description = "", color = "white", flash = true) {
		if (!this._isMounted) return;

		let next = this.state.info;
		next[name] = {
			type: "value",
			description: description,
			color: this.color(color),
			text: value.toString(),
			flash: flash
		};

		this._updated.push(name);

		this.setState({
			info: next
		});
	}

	event(text, color = "event", timeout = 1000, align = "left") {
		if (!this._isMounted) return;

		let now = Date.now().toString();

		let next = this.state.events;
		next[now] = {
			type: "event",
			color: this.color(color),
			text: text,
			timeout: timeout,
			align: align
		};

		this._eventTimeout = setTimeout(() => {
			this.remove(now, true);
		}, timeout);

		this.setState({
			events: next
		});
	}

	remove(name, isEvent = false) {
		if (!this._isMounted) return;

		let next = this.state[isEvent ? "events" : "info"];
		delete next[name];

		this.setState({
			[isEvent ? "events" : "info"]: next
		});
	}

	componentDidUpdate() {
		this._updated = [];
	}

	render() {
		const { info, events } = this.state;
		const { visible } = this.props;

		return (
			<div
				style={{
					transform: visible ? "translateX(0%)" : "translateX(100%)",
					opacity: visible ? 1 : 0,
					position: "absolute",
					top: 32,
					right: 0,
					zIndex: 5,
					// width:
					// 	Math.max(
					// 		maxNameLength + maxValueLength + 1,
					// 		maxEventLength
					// 	) * 8,
					// height: lines.length * 16 + 8,
					margin: 12,
					padding: "6px 8px",
					fontSize: 10,
					lineHeight: 1,
					borderRadius: 4,
					fontFamily: "monospace",
					color: "#FFFFFF",
					background: "#221f2866",
					textShadow: "0 0 4px #221f28",
					overflow: "hidden",
					transition: "all 0.3s",
					userSelect: "none"
				}}
			>
				<Global
					styles={css`
						@keyframes flash {
							from {
								filter: brightness(2);
								transform: scale(1.05);
							}
							to {
								filter: brightness(1);
								transform: scale(1);
							}
						}
						@keyframes fadein {
							from {
								opacity: 0;
							}
							to {
								opacity: 1;
							}
						}
						@keyframes fadeout {
							from {
								opacity: 1;
							}
							to {
								opacity: 0;
							}
						}
					`}
				/>
				<table style={{ width: "100%" }}>
					<tbody>
						{Object.keys(info).map(id => (
							<tr
								key={id}
								title={info[id].description}
								style={{
									textAlign: "right",
									cursor:
										info[id].description.length > 0
											? "help"
											: "auto"
								}}
							>
								<td>{id}</td>
								<td
									style={{
										paddingLeft: 8,
										textAlign: "left",
										color: info[id].color,
										animation:
											info[id].flash &&
											this._updated.indexOf(id) !== -1
												? "flash 1s"
												: ""
									}}
								>
									{info[id].text}
								</td>
							</tr>
						))}
						{Object.keys(events).map(id => (
							<tr key={id}>
								<td
									colSpan="2"
									style={{
										textAlign: events[id].align,
										color: events[id].color,
										animation: `fadeout 0.3s ease ${(
											events[id].timeout / 1000 -
											0.3
										).toFixed(1)}s forwards`
									}}
								>
									{events[id].text}
								</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		);
	}
}
