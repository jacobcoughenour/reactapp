import { Component } from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import { MediaControls, StatsOverlay } from "./";
import YouTubePlayer from "react-player/lib/players/YouTube";
import { Shortcuts } from "react-shortcuts";

const paddingHack = 200;
const PlaybackDeltaThreshold = 1200;

export default class MediaPlayer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			url: props.room.media[props.room.cur],
			start: props.room.start,
			pip: false,
			playing: false,
			volume: 1,
			muted: process.env.NODE_ENV === "development",
			fullscreen: false,
			theater: false,
			played: 0,
			playedSeconds: 0,
			loaded: 0,
			duration: 0,
			playbackRate: 1.0,
			loop: false,
			seeking: false,
			showStats: false,
			aspect: 9 / 16
		};

		this._startBuffer = Date.now();
		this._startTimeout = -1;
	}

	componentDidMount() {
		const { socket } = this.props;

		socket.on("fullsync", data => {
			this.setState({
				url: data.media[data.cur],
				playing: false,
				start: data.start
			});

			this._statsOverlay.event("fullsync");
		});

		this._statsOverlay.set(
			"ping",
			"ms",
			"Waiting for first Socket.io ping"
		);

		socket.on("pong", data => {
			this._statsOverlay.set(
				"ping",
				data + "ms",
				"latest Socket.io ping"
			);
		});

		socket.on("clocksync", () => {
			let now = Date.now();

			if (this.state.start - now > 0) {
				this.setState({ playing: false });

				clearTimeout(this._startTimeout);
				this._startTimeout = setTimeout(() => {
					this.setState({ playing: true });
				}, this.state.start - Date.now());

				return;
			}

			let playingAt =
				(this.player && this.player.getCurrentTime() | 0) * 1000;
			let shouldBeAt =
				now + (this.props.clock.getDelta() | 0) - this.state.start;

			let delta = playingAt - shouldBeAt;
			let norm = Math.abs(delta);

			if (norm > PlaybackDeltaThreshold) {
				// console.log(`\tover ${timeSyncThreshold}ms threshold, seeking to catch up...`);

				this._statsOverlay.event(
					`delta > ${PlaybackDeltaThreshold}ms thres`,
					"warn",
					2800,
					"left"
				);
				this._statsOverlay.event(
					"seeking to catch up...",
					"warn",
					2720,
					"left"
				);

				let prog = shouldBeAt / 1000 / this.state.duration;

				if (prog >= 1) this.setState({ playing: false });
				else this.player && this.player.seekTo(prog);
			}

			this._statsOverlay.remove("Playback Delta");
			this._statsOverlay.set(
				"Playback Delta",
				delta.toFixed(0) + "ms",
				`The current difference between the server and client playback time.\nIf delta is larger than the ${PlaybackDeltaThreshold}ms threshold, the player will try seeking to match the server playback time`,
				norm > PlaybackDeltaThreshold ? "bad" : "good"
			);
		});
	}

	ref = player => {
		this.player = player;
	};

	playPause = () => {
		this.setState({ playing: !this.state.playing });
	};

	skip = () => {
		this.props.socket.emit("skip");
	};

	setVolume = value => {
		this.setState({ volume: value });
	};

	toggleMute = () => {
		this.setState({ muted: !this.state.muted });
	};

	toggleTheaterMode = () => {
		this.props.roomview.toggleTheaterMode();
	};

	toggleFullscreen = () => {
		this.props.roomview.toggleFullscreen();
	};

	onSeekMouseDown = e => {
		this.setState({ seeking: true });
	};

	onSeekChange = value => {
		this.setState({ played: value });
	};

	onSeekMouseUp = value => {
		this.setState({ seeking: false });
		this.player.seekTo(value);
	};

	onReady = () => {
		// console.log(Date.now() - this._startBuffer);

		if (this.state.start - Date.now() > 0) {
			this.setState({ playing: false });

			clearTimeout(this._startTimeout);
			this._startTimeout = setTimeout(() => {
				this.setState({ playing: true });
			}, this.state.start - Date.now());
		} else {
			this.player.seekTo(
				(Date.now() - this.props.clock.getDelta() - this.state.start) /
					1000
			);
			this.setState({ playing: true });
		}

		this._statsOverlay.set("Player State", "Ready");
	};

	onStart = () => {
		this._statsOverlay.set("Player State", "Started");
	};

	onPlay = () => {
		// console.log(Date.now() - this._startBuffer);

		if (this.state.start <= Date.now());
		else this.setState({ playing: false });

		this._statsOverlay.set("Player State", "Playing");
	};

	onProgress = state => {
		if (!this.state.seeking) this.setState(state);
	};

	onDuration = duration => {
		this.setState({ duration });
	};

	onBuffer = () => {
		this._startBuffer = Date.now();
		this._statsOverlay.set("Player State", "Buffering");
	};

	onSeek = () => {
		this._statsOverlay.set("Player State", "Seeking");
	};

	onEnded = () => {
		this._statsOverlay.set("Player State", "Ended");
	};

	onError = e => {
		this._statsOverlay.event(`Player Error ${e}`, "error", 5000);
	};

	handleKeyboard = action => {
		if (action === "TOGGLE_STATS")
			this.setState({
				showStats: !this.state.showStats
			});
		else if (action === "TOGGLE_MUTE") this.toggleMute();
		else if (action === "PLAYPAUSE") this.playPause();
	};

	render() {
		const {
			url,
			playing,
			volume,
			muted,
			loop,
			playbackRate,
			pip,
			showStats
		} = this.state;
		const { fullscreen } = this.props.roomview.state;
		const { className, style } = this.props;

		const heightdiff = fullscreen ? 0 : this.props.heightdiff;

		return (
			<Shortcuts
				name="MEDIAPLAYER"
				handler={this.handleKeyboard}
				targetNodeSelector="body"
				className={className}
				css={[
					{
						// border: `solid 0px ${theme.palette.primary.main}`,
						// borderRadius: `${theme.shape.borderRadius}px`,
						// boxShadow: "0 0 32px #c78bff75",
						background: "#000000",
						position: "relative",
						// maxHeight: `calc((${heightdiff | 0}px) * 9 / 16)`,
						// height: "200px",
						width: "100%",
						overflow: "hidden",
						boxSizing: "border-box",
						outlineStyle: "none"
					},
					style
				]}
				ref={div => (this._parentdiv = div)}
			>
				<div
					style={{
						position: "relative",
						height: "100%",
						width: "100%",
						margin: "auto",
						maxWidth: `calc((100vh - ${heightdiff ||
							0}px) * 16 / 9)`,
						overflow: "hidden"
					}}
				>
					<div
						className="media-wrapper"
						style={{
							position: "relative",
							paddingBottom: "calc(100% * 9 / 16)"
						}}
					>
						<div
							className="player-container"
							style={{
								position: "absolute",
								top: -paddingHack,
								bottom: -paddingHack,
								width: "100%",
								overflow: "hidden"
							}}
						>
							<YouTubePlayer
								ref={this.ref}
								url={"http://www.youtube.com/watch?v=" + url}
								pip={pip}
								playing={playing}
								loop={loop}
								playbackRate={playbackRate}
								volume={volume}
								muted={muted}
								progressInterval={200}
								onReady={this.onReady}
								onStart={this.onStart}
								onPlay={this.onPlay}
								onProgress={this.onProgress}
								onDuration={this.onDuration}
								onBuffer={this.onBuffer}
								onSeek={this.onSeek}
								onEnded={this.onEnded}
								onError={this.onError}
								config={{
									youtube: {
										playerVars: {
											origin: window.location.href,
											rel: null
										}
									}
								}}
								height="100%"
								width="100%"
							/>
						</div>
					</div>
				</div>
				<div className="player-overlay">
					<MediaControls className="media-controls" player={this} />
					<StatsOverlay visible={showStats} player={this} />
				</div>
			</Shortcuts>
		);
	}
}
