import { Component } from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import IconButton from "@material-ui/core/IconButton";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import VolumeUpIcon from "@material-ui/icons/VolumeUp";
import VolumeOffIcon from "@material-ui/icons/VolumeOff";
import FullscreenIcon from "@material-ui/icons/Fullscreen";
import FullscreenExitIcon from "@material-ui/icons/FullscreenExit";
// import TheaterModeIcon from "@material-ui/icons/Crop169";
// import DefaultModeIcon from "@material-ui/icons/CropLandscape";
import ArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeftRounded";
import { Slider as OldSlider } from "react-player-controls";
import Slider from "@material-ui/lab/Slider";
import Tooltip from "@material-ui/core/Tooltip";
import { formatTime } from "../utils";

const PlayerButton = ({ style, children, ...props }) => (
	<Tooltip title={props.tooltip} aria-label={props.tooltip} placement="top">
		<IconButton
			css={[
				{
					// padding: "4px 4px",
					margin: "-16px -6px"
				},
				style
			]}
			{...props}
		>
			{children}
		</IconButton>
	</Tooltip>
);

const SliderBar = ({ value, style, ...props }) => (
	<div
		style={{
			width: `${value * 100}%`
		}}
		css={[
			{
				position: "absolute",
				background: "#878c88",
				borderRadius: 4,
				top: 0,
				bottom: 0,
				left: 0
			},
			style
		]}
		{...props}
	/>
);

const SliderHandle = ({ value, style, ...props }) => (
	<div
		style={{
			left: `${value * 100}%`
		}}
		css={[
			{
				position: "absolute",
				width: 16,
				height: 16,
				background: "#72d687",
				borderRadius: "100%",
				transform: "scale(1)",
				transition: "transform 0.2s",
				"&:hover": {
					transform: "scale(1.3)"
				},
				top: 0,
				marginTop: -4,
				marginLeft: -8
			},
			style
		]}
		{...props}
	/>
);

export default class MediaControls extends Component {
	constructor(props) {
		super(props);

		this.state = {
			mouseHover: false,
			volumeHover: false,
			volumeDrag: false
		};

		this._lastVolume = props.player.state.volume;
	}

	_handleMouseMove = () => {
		// show controls when mouse moves
		clearInterval(this._hideTimeout);
		this._hideTimeout = setTimeout(this._handleMouseLeave, 2500);
		if (!this.state.mouseHover) this._handleMouseEnter();
	};

	_handleMouseEnter = () => {
		this.setState({ mouseHover: true });
	};

	_handleMouseLeave = () => {
		clearInterval(this._hideTimeout);
		this.setState({ mouseHover: false });
	};

	_handleVolumeMouseEnter = () => {
		this.setState({ volumeHover: true });
	};

	_handleVolumeMouseLeave = () => {
		this.setState({ volumeHover: false });
	};

	_handleVolumeDragStart = () => {
		this._lastVolume = this.props.player.state.volume;
		this.setState({ volumeDrag: true });
	};

	_handleVolumeDragEnd = () => {
		this.setState({ volumeDrag: false });
	};

	_handleVolumeChange = val => {
		if (!isNaN(val)) {
			this.props.player.setState({
				muted: val === 0
			});
			// if dragged to 0, mute and restore volume
			this.props.player.setVolume(
				val === 0
					? this._lastVolume
					: Math.max(0, Math.min(1, val.toFixed(4)))
			);
		}
	};

	render() {
		const { className, style, player } = this.props;
		const { volumeHover, volumeDrag, mouseHover } = this.state;
		const {
			duration,
			loaded,
			muted,
			played,
			playing,
			volume,
			playedSeconds,
			seeking
		} = player.state;
		const { fullscreen, sidebaropen } = player.props.roomview.state;
		const { embedded } = player.props.roomview.props;

		return (
			<div
				className={className}
				onMouseEnter={this._handleMouseEnter}
				onMouseLeave={this._handleMouseLeave}
				css={[
					{
						background: "transparent",
						border: 0,
						outline: "none",
						width: "100%",
						height: "100%",
						padding: 0,
						".overlaygradient, .overlaytop, .overlaybottom": {
							position: "absolute",
							left: 0,
							right: 0
						},
						".overlaygradient": {
							opacity: 0,
							height: fullscreen ? 20 : 16,
							zIndex: 1,
							transition: "all 0.8s 0.4s"
						},
						".overlaysidegradient": {
							position: "absolute",
							opacity: 0,
							top: "50%",
							transform: "translate(66.6%,-50%)",
							height: 320,
							bottom: 0,
							width: fullscreen ? 56 : 48,
							transition: "all 0.8s 0.4s"
						},
						".overlaytop": {
							top: 0,
							margin: "12px 18px",
							zIndex: 3,
							opacity: 0,
							transition: "opacity 0.4s 0.4s"
						},
						".overlayright": {
							position: "absolute",
							right: 8,
							top: "50%",
							bottom: 0,
							transform: "translateY(-50%)",
							zIndex: 3,
							opacity: 0,
							width: fullscreen ? 40 : 32,
							transition: "opacity 0.4s 0.4s, width 0.4s 0.4s"
						},
						".overlaybottom": {
							bottom: 0,
							padding: 0,
							zIndex: 3,
							height: 2,
							transition: "height 0.4s 0.4s",
							".overlayprogress": {
								flex: 1,
								overflow: "hidden",
								cursor: "inherit",
								height: 2,
								margin: 0,
								borderRadius: 0,
								background: "rgba(255,255,255,0)",
								transition:
									"margin 0.2s ease 0.6s, height 0.2s ease 0.6s, borderRadius 0.15s 0.4s, background 0.3s 0.4s"
							},
							".overlaybuttons": {
								height: fullscreen ? 40 : 32,
								display: "flex",
								margin: "4px 8px 0px 8px",
								overflow: "hidden",
								borderRadius: 4,
								opacity: 0,
								transition: "opacity 0.15s 0.4s"
							}
						}
					},
					(!playing || mouseHover || volumeDrag) && {
						".overlaygradient": {
							height: fullscreen ? 72 : 64,
							opacity: 1,
							transition: "all 0.2s 0s"
						},
						".overlaysidegradient": {
							width: fullscreen ? 180 : 160,
							opacity: fullscreen ? 1 : 0,
							transition: "all 0.2s 0s"
						},
						".overlaytop": {
							opacity: 1,
							transition: "height 0.1s 0s"
						},
						".overlayright": {
							width: fullscreen ? 54 : 46,
							opacity: 1,
							transition: "opacity 0.05s 0s, width 0.1s 0s"
						},
						".overlaybottom": {
							height: fullscreen ? 54 : 46,
							transition: "height 0.1s 0s",
							".overlayprogress": {
								height: 4,
								margin: "0 8px",
								borderRadius: 4,
								background: "rgba(255,255,255,0.2)",
								transition:
									"margin 0.1s ease 0s, height 0.1s ease 0s, borderRadius 0.1s 0s, background 0.3s 0s"
							},
							".overlaybuttons": {
								opacity: 1,
								transition: "opacity 0.05s 0s"
							}
						}
					},
					style
				]}
			>
				<button
					className="overlaycenter"
					css={{
						position: "absolute",
						top: 0,
						left: 0,
						background: "transparent",
						height: "100%",
						width: "100%",
						zIndex: 2,
						border: 0,
						outline: "none"
					}}
					style={
						!playing || mouseHover || volumeDrag
							? {
									cursor: "inherit"
							  }
							: {
									cursor: "none"
							  }
					}
					// onClick={player.playPause}
					onMouseMove={this._handleMouseMove}
					onDoubleClick={player.toggleFullscreen}
				/>
				<div
					className="overlaygradient"
					css={{
						bottom: 0,
						background:
							"linear-gradient(0, rgba(34, 31, 40, 0.57), transparent)"
					}}
				/>
				<div
					className="overlaysidegradient"
					css={{
						right: 0,
						background:
							"radial-gradient(ellipse closest-side, rgba(34, 31, 40, 0.57), transparent)"
					}}
				/>
				<div
					className="overlaygradient"
					css={{
						top: 0,
						background:
							"linear-gradient(0, transparent, rgba(34, 31, 40, 0.57))"
					}}
				/>
				<div className="overlaytop">
					<div>
						{player.props.mediacache.hasOwnProperty(
							player.state.url
						) ? (
							<a
								style={{
									color: "#FFF",
									fontSize: fullscreen ? 20 : 16
								}}
								target="_blank"
								rel="noopener noreferrer"
								href={`https://www.youtube.com/watch?v=${player.state.url}`}
							>
								{
									player.props.mediacache[player.state.url]
										.title
								}
							</a>
						) : (
							"Loading..."
						)}
					</div>
					<div>
						{player.props.mediacache.hasOwnProperty(
							player.state.url
						) ? (
							<a
								style={{
									color: "#FFF",
									opacity: 0.8,
									fontSize: fullscreen ? 20 : 16
								}}
								target="_blank"
								rel="noopener noreferrer"
								href={
									player.props.mediacache[player.state.url]
										.channel.url
								}
							>
								{
									player.props.mediacache[player.state.url]
										.channel.title
								}
							</a>
						) : (
							""
						)}
					</div>
				</div>
				<div
					className="overlayright"
					onMouseEnter={() => {
						clearInterval(this._hideTimeout);
					}}
				>
					<IconButton
						onClick={player.props.roomview.openSidebar}
						style={{
							display:
								!sidebaropen && (embedded || fullscreen)
									? "inherit"
									: "none",
							top: "50%",
							transform: "translateY(-50%)"
						}}
					>
						<ArrowLeftIcon />
					</IconButton>
				</div>
				<div
					className="overlaybottom"
					onMouseEnter={() => {
						clearInterval(this._hideTimeout);
					}}
				>
					<OldSlider
						className="overlayprogress"
						isEnabled={false}
						onChangeStart={player.onSeekMouseDown}
						onChange={player.onSeekChange}
						onChangeEnd={player.onSeekMouseUp}
					>
						<SliderBar
							value={loaded.toFixed(3)}
							style={{
								background: "rgba(255,255,255,0.2)",
								transition: "width 0.5s"
							}}
						/>
						<SliderBar
							value={played.toFixed(3)}
							style={
								seeking
									? {
											background: "#fff"
									  }
									: {
											background: "#fff",
											transition: "width 0.2s linear"
									  }
							}
						/>
						<SliderHandle
							value={played.toFixed(3)}
							style={{
								transform: "scale(0)",
								background: "#fff",
								height: 12,
								width: 12
							}}
						/>
					</OldSlider>
					<div className="overlaybuttons">
						<PlayerButton
							tooltip={playing ? "pause" : "play"}
							onClick={player.playPause}
							style={{ padding: 18 }}
						>
							{playing ? (
								<PauseIcon
									fontSize={fullscreen ? "large" : "default"}
								/>
							) : (
								<PlayArrowIcon
									fontSize={fullscreen ? "large" : "default"}
								/>
							)}
						</PlayerButton>
						<PlayerButton
							tooltip={"skip to next video"}
							onClick={player.skip}
						>
							<SkipNextIcon
								fontSize={fullscreen ? "large" : "default"}
							/>
						</PlayerButton>
						<span
							css={{
								display: "flex",
								width: fullscreen ? 40 : 32,
								overflow: "visible",
								transition: "width 0.08s 0.4s"
							}}
							style={
								volumeHover || volumeDrag
									? {
											width: fullscreen ? 140 : 100,
											transition: "width 0.08s 0s"
									  }
									: {}
							}
							onMouseEnter={this._handleVolumeMouseEnter}
							onMouseLeave={this._handleVolumeMouseLeave}
						>
							<PlayerButton
								tooltip={muted ? "unmute (m)" : "mute (m)"}
								onClick={player.toggleMute}
							>
								{muted || volume === 0 ? (
									<VolumeOffIcon
										fontSize={
											fullscreen ? "large" : "default"
										}
									/>
								) : (
									<VolumeUpIcon
										fontSize={
											fullscreen ? "large" : "default"
										}
									/>
								)}
							</PlayerButton>
							<Slider
								style={{
									margin: fullscreen
										? "20px 10px"
										: "16px 8px",
									opacity: volumeHover || volumeDrag ? 1 : 0,
									transition:
										volumeHover || volumeDrag
											? "width 0.08s 0s"
											: "opacity 0.08s 0.4s"
								}}
								min={0}
								max={1}
								step={0.05}
								value={muted ? 0 : volume}
								onChange={(event, val) =>
									this._handleVolumeChange(val)
								}
								onDragStart={this._handleVolumeDragStart}
								onDragEnd={this._handleVolumeDragEnd}
							/>
						</span>
						<span
							css={{
								padding: fullscreen ? "10px 14px" : "8px 12px",
								fontSize: fullscreen ? 18 : 14,
								color: "white",
								flex: 1,
								opacity: 0.7,
								userSelect: "none"
							}}
						>
							<time>{formatTime(playedSeconds)}</time>
							<span
								style={{
									opacity: 0.7
								}}
							>
								{" / "}
								<time>{formatTime(duration)}</time>
							</span>
						</span>
						{/* {!fullscreen && (
							<PlayerButton
								tooltip={
									fullscreen
										? "Default view (t)"
										: "Theater mode (t)"
								}
								onClick={player.toggleTheatermode}
								style={{
									padding: "4px 8px 4px 5px"
								}}
							>
								<TheaterModeIcon
									fontSize={fullscreen ? "large" : "default"}
								/>
							</PlayerButton>
						)} */}
						<PlayerButton
							tooltip={
								fullscreen
									? "Exit full screen (f)"
									: "Full screen (f)"
							}
							onClick={player.toggleFullscreen}
							style={
								{
									// padding: "4px 12px 4px 5px"
								}
							}
						>
							{fullscreen ? (
								<FullscreenExitIcon
									fontSize={fullscreen ? "large" : "default"}
								/>
							) : (
								<FullscreenIcon
									fontSize={fullscreen ? "large" : "default"}
								/>
							)}
						</PlayerButton>
					</div>
				</div>
			</div>
		);
	}
}
