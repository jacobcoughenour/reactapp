var schema;
try {
	schema = require("../server/build/graphql/schema.json");
} catch (e) {
	console.warn("\x1b[33mESLint Warning\x1b[0m");
	console.warn(
		"\x1b[33m\nThe schema.json file used for linting GraphQL queries is missing from the server build directory!\nGraphQL linting will be disabled until the server builds the schema.json file and the client dev server is reloaded.\x1b[0m"
	);
}

module.exports = {
	plugins: ["react-redux", "react-app", "graphql"],
	extends: [
		"plugin:react-app/recommended",
		"plugin:react-redux/recommended",
		"plugin:prettier/recommended"
	],
	rules: {
		"react-redux/connect-prefer-named-arguments": "off",
		"prettier/prettier": "warn",
		"graphql/template-strings": schema
			? [
					"warn",
					{
						env: "apollo",
						schemaJson: schema
					}
			  ]
			: 0
	}
};
